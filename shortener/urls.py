from django.urls import path
from .views import create_short_url, short_url_redirect

urlpatterns = [
    path('home/', create_short_url, name="home"),
    path("s/<str:slugs>/", short_url_redirect, name="redirect")
]
