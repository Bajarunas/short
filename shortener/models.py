from django.db import models


class ShortUrl(models.Model):
    real_url = models.CharField(max_length=124)
    short_url = models.CharField(max_length=20)

    def __str__(self):
        return self.real_url
