from django.shortcuts import render, redirect
from django.contrib import messages
from .models import ShortUrl
import random
import string


def create_short_url(request):
    if request.method == 'POST':
        real_url = request.POST.get('real_url')
        slug = ''.join(random.choice(string.ascii_letters)for x in range(5))
        while ShortUrl.objects.filter(short_url=slug).exists():
            slug = ''.join(random.choice(string.ascii_letters)for x in range(5))
        short_url_object = ShortUrl(real_url=real_url, short_url=slug)
        short_url_object.save()
        messages.success(
            request, f'Short url has been created for {real_url}!')
        short_url_dict = {
            'short': "http://127.0.0.1:8000/s/"+short_url_object.short_url+"/"}
        return render(request, 'index.html', short_url_dict)
    return render(request, 'index.html')


def short_url_redirect(request, slugs):
    data = ShortUrl.objects.get(short_url=slugs)
    real_url = "https://"+str(data.real_url)
    return redirect(real_url)
