# Project Short Url
#### Created By Liudvikas Bajarūnas


## How to run the project:

#### 1. Clone the project.

`git clone https://gitlab.com/Bajarunas/short.git`

#### 2. Go to cloned folder and install the requirements.
`cd short`
`pip3 install -r requirements.txt` 

#### 3. Run the server.
`python3 manage.py runserver`



## How to use the Short Url:

1. Go to http://127.0.0.1:8000/home/ 
2. Enter an url to the input. (Example: www.google.com, www.gmail.com)
3. Submit the form, get the short url, either copy the url or press on the link, to be redirected to the real url.
4. That's it!


### Super user account
username: admin
password: admin